package com.example.supercaculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //1. Crear los objetos que se relacionaran con los botones del layout
    MaterialButton boton0, boton1 , boton2 , boton3 ,
            boton4 ,boton5 , boton6 , boton7 , boton8 , boton9,
            botonParentesisi, botonParentesisii, botonC, botonAc;
    TextView expresion, resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        expresion= findViewById(R.id.expresion);
        resultado= findViewById(R.id.resultado);

        //2. Relacionar el objeto Boton1 con el elemento cuyo id sea boton_1
        boton0= findViewById(R.id.boton_0);
        boton1= findViewById(R.id.boton_1);
        boton2= findViewById(R.id.boton_2);
        boton3= findViewById(R.id.boton_3);
        boton4= findViewById(R.id.boton_4);
        boton5= findViewById(R.id.boton_5);
        boton6= findViewById(R.id.boton_6);
        boton7= findViewById(R.id.boton_7);
        boton8= findViewById(R.id.boton_8);
        boton9= findViewById(R.id.boton_9);
        botonParentesisi= findViewById(R.id.boton_parentesis_i);
        botonParentesisii= findViewById(R.id.boton_parentesis_d);
        botonC= findViewById(R.id.boton_c);
        botonAc= findViewById(R.id.boton_ac);


        //3. Convertir el boton1 en un listener

        boton0.setOnClickListener(this);
        boton1.setOnClickListener(this);
        boton2.setOnClickListener(this);
        boton3.setOnClickListener(this);
        boton4.setOnClickListener(this);
        boton5.setOnClickListener(this);
        boton6.setOnClickListener(this);
        boton7.setOnClickListener(this);
        boton8.setOnClickListener(this);
        boton9.setOnClickListener(this);
        botonParentesisi.setOnClickListener(this);
        botonParentesisii.setOnClickListener(this);
        botonC.setOnClickListener(this);
        botonAc.setOnClickListener(this);


    }

    @Override
    public void onClick (View view){
        Opciones(view);
    }


    private void Opciones(View view){
        //switch (String.valueOf(txtBoton.charAt(txtBoton.toCharArray().length - 1))) {
        //crear un objeto que represente el boton al cual se le ha hecho click
        MaterialButton boton = (MaterialButton) view;

        switch (boton.getText().toString()) {
            case "C":
                this.expresion.setText("");
                break;
            case "AC":
                this.expresion.setText("");
                this.resultado.setText("");
                break;
            default:
                String texto = (String) this.expresion.getText();

                texto +=  boton.getText().toString();
                texto = PrimeraLetraCero(texto);
                this.expresion.setText(texto);

                break;
        }
    }

    private String PrimeraLetraCero(String texto){
        if(texto.charAt(0) == '0'){
            StringBuffer sbf = new StringBuffer(texto);
            texto =  sbf.deleteCharAt(0).toString();
        }
        return texto;
    }

}